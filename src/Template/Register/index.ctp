<?php $this->layout = false;?>
<!doctype html>
<html lang="en">
  
<!-- Mirrored from getbootstrap.com/docs/4.1/examples/sign-in/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Nov 2018 23:41:52 GMT -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/signin.css" rel="stylesheet">
    <style>
    .error-message, .error{
      color:red;
    }
    .error-message ul{
      list-style: none;
    }
    </style>
  </head>

  <body class="text-center">
    <?=$this->Form->create($user, ['class' => 'form-signin']);?>
      <img class="mb-4" src="assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Please sign up</h1>
      <?=$this->Flash->render()?>
      <br>
      <label for="firstname" class="">First Name</label>
      <?=$this->Form->control('firstname', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "placeholder"=>"First name", "required", "autofocus"]);?>

      <label for="lastname" class="">Last Name</label>
      <?=$this->Form->control('lastname', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "placeholder"=>"Last name", "required", "autofocus"]);?>

      <label for="email" class="">Email</label>
      <?=$this->Form->control('email', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "placeholder"=>"Email", "required", "autofocus"]);?>

      <label for="password" class="">Password</label>
      <?=$this->Form->control('password', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "required", "autofocus"]);?>

      <label for="password" class="">Confirm Password</label>
      <?=$this->Form->control('password2', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "required", "autofocus", "type"=>"password"]);?>

      <label for="username" class="">Username</label>
      <?=$this->Form->control('username', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "placeholder"=>"First name", "required", "autofocus"]);?>

      <label for="phone" class="">Phone</label>
      <?=$this->Form->control('phone', ['templates' => ['inputContainer' => '{{content}}'], 'label' => false, 'class' => 'form-control', "placeholder"=>"Phone", "required", "autofocus"]);?>


      <?=$this->Form->button(__('Submit <i class="m-icon-swapright m-icon-white"></i>'), ['class' => 'btn btn-lg btn-primary btn-block', 'id' => '']);?>
      <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
      <?=$this->Form->end()?>
  </body>

<!-- Mirrored from getbootstrap.com/docs/4.1/examples/sign-in/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Nov 2018 23:41:53 GMT -->
</html>
